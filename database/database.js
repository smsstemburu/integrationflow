const Sequelize = require("sequelize");

const sequelize = new Sequelize("TESTDB", "root", "root", {
  dialect: "mysql",
  host: "localhost",
});

module.exports = sequelize;
