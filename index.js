var express = require("express");
var dataRoute = require("./routes/dataRoute");
// const produce = require("./producer");
const consume = require("./consumer");

var app = express();
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

app.use(dataRoute);

// call the `produce` function and log an error if it occurs
// produce().catch((err) => {
//   console.error("error in producer: ", err);
// });

// start the consumer, and log any errors
consume().catch((err) => {
  console.error("error in consumer: ", err);
});

app.listen("9000", (err) => {
  if (err) {
    console.log(err.message);
  } else {
    console.log("Server is up and running at port 9000");
  }
});
