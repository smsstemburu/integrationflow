const { Kafka } = require("kafkajs");

const sequelize = require("./database/database");

const Customer = require("./models/testData");

// the client ID lets kafka know who's producing the messages
const clientId = "my-app";
// we can define the list of brokers in the cluster
const brokers = ["localhost:9092"];

const kafka = new Kafka({ clientId, brokers });

const topic = "newTopic";

// the kafka instance and configuration variables are the same as before

// create a new consumer from the kafka client, and set its group ID
// the group ID helps Kafka keep track of the messages that this client
// is yet to receive
const consumer = kafka.consumer({ groupId: clientId });

const consume = async () => {
  // first, we wait for the client to connect and subscribe to the given topic
  await consumer.connect();
  await consumer.subscribe({ topic });
  await consumer.run({
    // this function is called every time the consumer gets a new message
    eachMessage: async ({ message }) => {
      //   here, we just log the message to the standard output
      sequelize
        .sync()
        .then((result) => {
          console.log(result);
          return Customer.create({ data: JSON.parse(message.value).value });
        })
        .then((customer) => {
          console.log("First customer created: ", customer);
        })

        .catch((err) => {
          console.log(err);
        });
      console.log(`received message: ${JSON.parse(message.value).value}`);
      //   console.log(JSON.parse(message.value).value);
    },
  });
};

module.exports = consume;
