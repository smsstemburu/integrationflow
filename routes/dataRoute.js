var express = require("express");

const messageContoller = require("../controllers/messageController");
var router = express.Router();
var app = express();

app.use(express.json());

// http://localhost:9000/dataRoute
router.post("/dataRoute", messageContoller);

module.exports = router;

// const express = require("express");

// const router = express.Router();
// const app = express();

// //Here we are configuring express to use body-parser as middle-ware.
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

// const data = router.post("/handleData", (request, response) => {
//   //code to perform particular action.
//   //To access POST variable use req.body()methods.
//   console.log(request.body);
// });

// // add router in the Express app.
// app.use("/", router);

// module.exports = data;
