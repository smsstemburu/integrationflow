const Sequelize = require("sequelize");

const sequelize = require("../database/database");

const Customer = sequelize.define("customer", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },

  data: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

module.exports = Customer;
